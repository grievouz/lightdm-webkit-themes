const path = require('path');
const webpack = require('webpack');
const { TsConfigPathsPlugin } = require('awesome-typescript-loader');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const Stylish = require('webpack-stylish');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    mode: 'development',
    devtool: 'source-map',
    stats: 'none',
    target: 'web',

    entry: {
        bundle: './src/arch/index.tsx'
    },

    output: {
        filename: 'js/[name].js',
        path: path.resolve(__dirname, 'public/arch')
    },

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.scss'],
        plugins: [new TsConfigPathsPlugin /* { configFileName, compiler } */()]
    },

    module: {
        rules: [
            {
                test: /\.(tsx|ts)$/,
                loader: 'awesome-typescript-loader',
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: MiniCssExtractPlugin.loader },
                    {
                        loader: 'css-loader',
                        options: {
                            minimize: true
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            includePaths: [
                                path.resolve(__dirname, 'src/arch/scss')
                            ]
                        }
                    }
                ]
            }
        ]
    },

    plugins: [
        new MiniCssExtractPlugin({
            filename: '/css/[name].css'
        }),
        new webpack.NamedModulesPlugin(),
        new Stylish(),
        new CopyWebpackPlugin([{ from: 'src/arch/static', to: '.' }], {})
    ]
};
