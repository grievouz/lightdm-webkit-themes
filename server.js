const express = require('express');
const path = require('path');
const reload = require('reload');
const chokidar = require('chokidar');
const http = require('http');
const modifyResponse = require('express-modify-response');
const cheerio = require('cheerio');
const proxy = require('express-http-proxy');
/* 
    Absolute root path absolute 
*/
const root = path.resolve(__dirname, './dist');

const app = express();
/* 
    Allows to relaod the page 
*/
const connection = reload(app);
/* 
    Creating HTTP server 
*/
const server = http.createServer(app);

/* 
    Proxy graphql endpoint 
*/
app.use('/api', proxy('http://127.0.0.1:8001/'));
/* 
    Modifys HTML files and injects the reload script 
*/
app.use(
    modifyResponse(
        (req, res) => {
            const type = req.originalUrl.split('.').pop();

            if (type === 'html' || type === 'htm' || type.includes('/'))
                return true;
            return false;
        },
        (req, res, body) => {
            const query = cheerio.load(body.toString());
            query('head').append(
                `<!-- Injected by server -->
                <script src="/reload/reload.js"></script>
                <script src="/mock.js"></script>`
            );

            return query.html();
        }
    )
);
/* 
    Serves files 
*/
app.use(express.static(root));

/* 
    Reloading after file change and after timeout 
    to counter constant relaoding while compiling
*/
let timeout;
chokidar
    .watch(root, {
        persistent: true
    })
    .on('change', ev => {
        if (timeout) clearTimeout(timeout);
        timeout = setTimeout(() => {
            console.log(
                '\x1b[0m\x1b[34m🛈',
                '\x1b[37m',
                'File change detected Server reloading',
                '\x1b[0m\n'
            );
            connection.reload();
        }, 200);
    });

/* 
    Starting server at port 9090 
*/
server.listen(9090, err => {
    if (err) return console.log(err);
    else
        console.log(
            '\x1b[0m\x1b[34m🛈',
            '\x1b[37m',
            'Server running at:',
            `\x1b[4mhttp://127.0.0.1:9090/`,
            '\x1b[0m'
        );
});
