import './index.scss';

import { h, render } from 'preact';
import { App } from 'arch/components/containers/app/app.model';

render(<App />, document.body);
