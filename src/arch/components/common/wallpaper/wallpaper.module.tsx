import './wallpaper.style.scss';

import { h } from 'preact';
import { Logo } from 'arch/components/common/logo/logo.module';
import { IWallpaperTheme } from 'arch/services/background.service';

export const Wallpaper = (props: { theme: IWallpaperTheme }) => {
    return (
        <section className={'wallpaper'}>
            <div
                className={'wallpaper__background__top'}
                style={{ background: props.theme.top.background }}
            />
            <div
                className={'wallpaper__background__bottom'}
                style={{ background: props.theme.bottom.background }}
            />

            <div className={'wallpaper__text --top'}>
                <section
                    className={'wallpaper__text__content'}
                    style={{ color: props.theme.bottom.logo }}
                    ref={(node: HTMLElement) => {
                        if (node) {
                            node.innerHTML = props.theme.top.text;
                        }
                    }}
                />
            </div>

            <Logo
                className={'wallpaper__logo'}
                topColor={props.theme.top.logo}
                bottomColor={props.theme.bottom.logo}
            />

            <div className={'wallpaper__text --bottom'}>
                <section
                    className={'wallpaper__text__content'}
                    style={{ color: props.theme.bottom.logo }}
                    ref={(node: HTMLElement) => {
                        if (node) {
                            node.innerHTML = props.theme.bottom.text;
                        }
                    }}
                />
            </div>
        </section>
    );
};
