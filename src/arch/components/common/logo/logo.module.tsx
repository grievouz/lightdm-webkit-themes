import './logo.style';

import { h } from 'preact';

export const Logo = (props: {
    className: string;
    topColor: string;
    bottomColor: string;
}) => {
    return (
        <svg
            className={props.className}
            viewBox="0 0 324 324"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <mask
                id="mask0"
                mask-type="alpha"
                maskUnits="userSpaceOnUse"
                x="0"
                y="0"
                width="324"
                height="324"
            >
                <path
                    fill-rule="evenodd"
                    clip-rule="evenodd"
                    d="M161.97 0C147.546 35.3564 138.846 58.4836 122.787 92.789C132.633 103.224 144.719 115.376 164.346 129.1C143.245 120.419 128.852 111.703 118.095 102.659C97.5429 145.536 65.3433 206.613 0 324C51.3576 294.356 91.1692 276.08 128.272 269.106C126.678 262.255 125.773 254.845 125.834 247.112L125.895 245.467C126.71 212.57 143.826 187.272 164.102 188.99C184.378 190.708 200.139 218.785 199.324 251.682C199.17 257.872 198.472 263.827 197.252 269.35C233.951 276.528 273.337 294.757 324 324C314.01 305.612 305.094 289.036 296.579 273.249C283.166 262.856 269.176 249.328 240.639 234.684C260.253 239.779 274.297 245.659 285.244 252.23C198.669 91.0722 191.658 69.6577 161.97 0Z"
                    fill="white"
                />
            </mask>
            <g mask="url(#mask0)">
                <rect width="324" height="162" fill={props.topColor} />
                <rect
                    y="162"
                    width="324"
                    height="162"
                    fill={props.bottomColor}
                />
            </g>
        </svg>
    );
};
