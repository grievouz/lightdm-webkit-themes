import './app.style';

import { h, Component } from 'preact';
import {
    getNewRandomWallpaperTheme,
    IWallpaperTheme
} from 'arch/services/background.service';
import { Wallpaper } from 'arch/components/common/wallpaper/wallpaper.module';

interface AppStates {
    wallpaperTheme?: IWallpaperTheme;
    input: {
        placeholder: string;
        value: string;
    };
}

export class App extends Component<{}, AppStates> {
    private _inputRef?: HTMLInputElement;

    constructor(props: {}) {
        super(props);

        this.state = {
            input: {
                placeholder: 'username',
                value: ''
            }
        };

        this.setNewBackground();

        setInterval(() => {
            this.setNewBackground();
        }, 1000 * 15);

        window.onload = () => {
            if (this._inputRef) {
                this._inputRef.focus();
            }
        };

        window.onkeydown = (ev: KeyboardEvent) => {
            if (ev.keyCode === 13) {
                this.authenticateStep(this.state.input.value);
            }
        };

        window.authentication_complete = () => {
            if (lightdm.is_authenticated) {
                lightdm.login(lightdm.authentication_user, null);
            }
        };
    }

    private setNewBackground(): void {
        this.setState({
            ...this.state,
            wallpaperTheme: getNewRandomWallpaperTheme()
        });
    }

    private handleInputRef = (inputRef: HTMLInputElement): void => {
        inputRef.onblur = () => {
            inputRef.focus();
        };

        this._inputRef = inputRef;
    };

    private authenticateStep(input: string) {
        if (!lightdm.in_authentication) {
            lightdm.authenticate(input);

            this.setState({
                input: {
                    placeholder: 'password',
                    value: ''
                }
            });
        } else if (!lightdm.authentication_user) {
            lightdm.respond(input);

            this.setState({
                input: {
                    placeholder: 'password',
                    value: ''
                }
            });
        } else {
            lightdm.respond(input);

            this.setState({
                input: {
                    placeholder: 'username',
                    value: ''
                }
            });
        }
    }

    public render() {
        if (this.state.wallpaperTheme)
            return (
                <main>
                    <Wallpaper theme={this.state.wallpaperTheme} />

                    <style>
                        {`
                            main > input::placeholder{
                                color: ${this.state.wallpaperTheme.bottom.logo}
                            }
                        `}
                    </style>

                    <input
                        ref={this.handleInputRef}
                        spellcheck={false}
                        style={{
                            textShadow:
                                '0 0 0 ' + this.state.wallpaperTheme.bottom.logo
                        }}
                        value={this.state.input.value}
                        placeholder={this.state.input.placeholder}
                        type={
                            this.state.input.placeholder === 'password'
                                ? 'password'
                                : 'text'
                        }
                        onInput={ev => {
                            this.setState({
                                input: {
                                    ...this.state.input,
                                    value: (ev.currentTarget as HTMLInputElement)
                                        .value
                                }
                            });
                        }}
                    />
                </main>
            );
        return null;
    }
}
