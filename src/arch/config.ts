export const config = {
    primaryColor: '#101010',
    secondaryColor: '#ededed',

    wallpapers: [
        {
            top: 'makepkg',
            bottom: 'not war'
        },
        {
            top: '',
            bottom: 'welcome ~/'
        },
        {
            top: '',
            bottom: 'better pacman than unpacwomen'
        },
        {
            top: '',
            bottom: 'if you can read this,<br/>xorg is still working'
        },
        {
            top: '',
            bottom: 'without Arch in your life<br/>you can not boot your wife!'
        },
        {
            top: '',
            bottom: 'no bugs, just tons of features'
        },
        {
            top: '',
            bottom: 'read the f***ing wiki'
        },
        {
            top: '',
            bottom: '--force now, think later!'
        },
        {
            top: '',
            bottom: "if it ain't broke,<br/> it ain't fun either"
        },
        {
            top: '',
            bottom: 'have you pacman -Syu today?'
        },
        {
            top: '',
            bottom: 'the least worst linux distribution'
        },
        {
            top: '',
            bottom: 'install once, use forever'
        },
        {
            top: '',
            bottom: 'no wombats were harmed<br/>during development'
        },
        {
            top: '',
            bottom:
                'where 30 minutes old<br/>packages are considered<br/><u>stable<u/>'
        },
        {
            top: '',
            bottom: "keep rollin' rollin' rollin'"
        },
        {
            top: '',
            bottom:
                'Q: How do you know someone is running Arch?<br/>A: They will tell you.'
        },
        {
            top: '',
            bottom: 'a little effort followed by<br/>profound laziness'
        }
    ]
};
