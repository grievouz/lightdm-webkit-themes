import { config } from 'arch/config';

const getRandomInt = (min: number, max: number) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};
const getRandomColor = (): ColorHex => {
    const randomInt = getRandomInt(0, 1);

    if (randomInt === 0) {
        return config.primaryColor;
    } else if (randomInt === 1) {
        return config.secondaryColor;
    }

    throw new Error('Color selector out of range!');
};
const getCompositeColor = (color: ColorHex): ColorHex => {
    return color === config.primaryColor
        ? config.secondaryColor
        : config.primaryColor;
};

interface IWallpaperText {
    top: ColorHex;
    bottom: ColorHex;
}
let lastText: IWallpaperText | undefined;
const getNewRandomWallpaperText = (): IWallpaperText => {
    const textHolder =
        config.wallpapers[getRandomInt(0, config.wallpapers.length - 1)];

    if (textHolder !== lastText) {
        lastText = textHolder;

        return textHolder;
    } else {
        return getNewRandomWallpaperText();
    }
};

interface IWallpaperBackground {
    top: ColorHex;
    bottom: ColorHex;
}
let lastBackground: IWallpaperBackground | undefined;
const getNewRandomWallpaperBackground = (): IWallpaperBackground => {
    const backgroundHolder = {
        top: getRandomColor(),
        bottom: getRandomColor()
    };

    if (backgroundHolder !== lastBackground) {
        lastBackground = backgroundHolder;

        return backgroundHolder;
    } else {
        return getNewRandomWallpaperBackground();
    }
};

interface IWallpaperLogo {
    top: ColorHex;
    bottom: ColorHex;
}
const getRandomWallpaperLogo = (
    wallpaperBackground: IWallpaperBackground
): IWallpaperLogo => {
    return {
        top: getCompositeColor(wallpaperBackground.top),
        bottom: getCompositeColor(wallpaperBackground.bottom)
    };
};

export const getNewRandomWallpaperTheme = (): IWallpaperTheme => {
    const wallpaperText = getNewRandomWallpaperText();
    const wallpaperBackground = getNewRandomWallpaperBackground();
    const wallpaperLogo = getRandomWallpaperLogo(wallpaperBackground);

    return {
        top: {
            background: wallpaperBackground.top,
            logo: wallpaperLogo.top,
            text: wallpaperText.top
        },
        bottom: {
            background: wallpaperBackground.bottom,
            logo: wallpaperLogo.bottom,
            text: wallpaperText.bottom
        }
    };
};

type ColorHex = string;

export interface IWallpaperTheme {
    top: {
        background: ColorHex;
        logo: ColorHex;
        text: string;
    };
    bottom: {
        background: ColorHex;
        logo: ColorHex;
        text: string;
    };
}
